public class Pesel {

    private String pesel;

    public Pesel(String pesel){
        this.pesel = pesel;
    }

    public boolean checkSum() {
        int[] weights = {1, 3, 7, 9, 1, 3, 7 ,9 ,1 ,3};
        if (pesel.length() != 11) {
            return false;
        }
        int sum = 0;
        for (int index = 0; index < 10; index++) {
            sum += Integer.parseInt(pesel.substring(index, index+ 1)) * weights[index];
        }
        int checkDigit = Integer.parseInt(pesel.substring(10, 11));
        sum %= 10;
        sum = 10 - sum;
        sum %= 10;

        return (sum == checkDigit);
    }

    public String getSex() {
        if (pesel.length() != 11) {
            return "Pesel jest nieprawidłowy!";
        }
        int number = Integer.parseInt(pesel.substring(9,10));
        if( number % 2 == 1) {
            return "Mężczyzna";
        }
        else {
            return "Kobieta";
        }
    }
}